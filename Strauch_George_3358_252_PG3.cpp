//  Author: George Strauch
//  serial number: 54
//  Due Date: March 6 2019
//  Programming Assignment  3
//  Spring 2019 - CS 3358 - 252
//  Instructor: Husain	Gholoom
//
//  object and implementation of MyInt class


#include <iostream>
#include <vector>
#include <cmath>
#include <sstream>

using namespace std;

// this is the class for MyInt
// the functions will be implemented separately
class MyInt
{
public:
    MyInt();
    void isMultiple();
    void sumOfDigitsOddOrEven();
    void getSqrt();
    void isPrime();
    void isPerfect();
    bool validateInput(string strValToValidate);
    void all();

private:
    // this is the actual value of of the data that is being dealt with
    int value;
    vector<int> getFactors();
    void setValue(int val);
};


// default constructor
// initializes the value field
MyInt::MyInt () { this->value = 0; }


// this function will get the factors of the value
// this function is a just helper function to other
// functions so it can be private
//
// returns a vector containing the factors of the value
vector<int> MyInt::getFactors()
{
   vector<int> factors;
   // checks all integers less than or equal to the value
    for (int i = 1; i <= sqrt(this->value); ++i) {
        if (value % i == 0)
        {
            factors.push_back(i);
            // dont just append 1 before because value could equal 0
            if (i*i != this->value && i != 1) { factors.push_back(this->value / i); }
        }
    }
    return factors;
}


// this function will determine if the value is a
// multiple of 7, 11, or 13
void MyInt::isMultiple ()
{
    bool isMult = false;
    string strFactors = "";

    vector<int> factors = this->getFactors();

    for (int i = 0; i < factors.size(); ++i) {
        if (factors[i] == 7)
        {
            isMult = true;
            strFactors += " 7";
        }
        else if (factors[i] == 11)
        {
            if (isMult) { strFactors += ", "; }
            else { strFactors += " "; }
            strFactors += "11";
            isMult = true;
        }
        else if (factors[i] == 13)
        {
            if (isMult) { strFactors += ", "; }
            else { strFactors += " "; }
            isMult = true;
            strFactors += "13";
        }
    }

    if (isMult)
    {
        cout << this->value << " is a multiple of:" << strFactors << '\n';
    }
    else
    {
        cout << this->value << " is not a multiple of 7, 11, or 13 \n";
    }
}


// this function will add the digits up and print
// if the sum is odd or even
void MyInt::sumOfDigitsOddOrEven ()
{
    string strOfVal;
    ostringstream tmp;  // this should not cause compiler problems
    tmp << this->value;
    int sum = 0;
    strOfVal = tmp.str();

    for (int i = 0; i < strOfVal.length(); ++i) {
        sum += ((int)strOfVal[i]-48);
    }

    if (sum % 2 == 0)
    {
        cout << "The sum of the digits of " << this->value << " is even \n";
    }
    else
    {
        cout << "The sum of the digits of " << this->value << " is odd \n";
    }

}


// just prints the square root of the value
void MyInt::getSqrt ()
{
    cout << "The square root of " << this->value << " is: " << sqrt(this->value) << '\n';
}


// determine if the value is prime
// a number is prime if it has no factors
// 0 however is not a prime
void MyInt::isPrime ()
{
    if (this->getFactors().size() == 1)
    {
        cout << this->value << " is prime \n";
    }
    else
    {
        cout << this->value << " is not prime \n";
    }
}


// this function tests to see if the value is a perfect number
// a perfect number is a number where the sum of its factors is equal to the number
void MyInt::isPerfect()
{
    vector<int> factors = getFactors();
    int sum = 0;

    for (int i = 0; i < factors.size(); ++i) {
        sum += factors[i];
    }

    if (sum == this->value)
    {
        cout << this->value << " is a perfect number \n";
    }
    else
    {
        cout << this->value << " is not a perfect number \n";
    }
}


// when the user cins the value for the MyInt object, its easiest to
// take it as a string to validate it, then convert it to a int
// this function ensures that the user input is an integer
//
// param : string value of the integer
bool MyInt::validateInput (string strValToValidate)
{
    bool invalid = false;
    for (int i = 0; i < strValToValidate.length(); ++i) {
        if (strValToValidate[i] - 48 > 9 || strValToValidate[i] - 48 < 0)
        {
            invalid = true;
            break;
        }
    }

    if (!invalid){
        // cvt string of numbers to int type
        int intVal = 0;
        stringstream strStream(strValToValidate);
        strStream >> intVal;
        setValue(intVal);
        return true;
    }

    // only gets here if input is invalid
    cout << "That was invalid input, try again...\n";
    return false;
}


// this is the function to set the value
// is private function because it is only called by
// validate input function after it is validated
void MyInt::setValue(int val)
{
    this->value = val;
}


// tests all functionality of MyInt object
// does not ask user to set value because it should already be set
void MyInt::all()
{
    this->isPrime();
    this->isMultiple();
    this->sumOfDigitsOddOrEven();
    this->getSqrt();
    this->isPerfect();
}
// --- end implementation of myInt class ---


// this function will be used my the main function to validate
// user input of which option of a switch statement is valid
//
// returns integer between 1-8
int getOption()
{
    string option;
    while (true)
    {
        cin >> option;
        cout <<"\n";
        if (option.size() != 1)
        {
            cout << "Invalid input, try again...\n";
            continue;
        }
        else if (option[0] < 49 || option[0] > 56){
            cout << "Invalid input, try again...\n";
            continue;
        }
        else { return option[0] - 48; }
    }
}


// ths is the main function of the program
int main()
{
    MyInt myIntObject = MyInt();

    cout << "\nWelcome to my Integer Manipulation Program\n\n";
    string userInput;

    // first we must initialize the variable
    cout << "Please enter an integer \n";
    do {
        cin >> userInput;
    }
    while (!myIntObject.validateInput(userInput));

    while (true) {
        cout << "Enter the option you would like to do\n"
             << "[1] Test if the number is a multiple of 7, 11, 13\n"
             << "[2] See if the sum of the digits is odd or even\n"
             << "[3] Get the square root value of the number\n"
             << "[4] Test if the number is prime\n"
             << "[5] Test if the number is a perfect number\n"
             << "[6] ALL THE ABOVE\n"
             << "[7] Set a new Number\n"
             << "[8] Exit the Program\n\n";

        switch (getOption()) {
            case 1:
                myIntObject.isMultiple();
                break;

            case 2:
                myIntObject.sumOfDigitsOddOrEven();
                break;

            case 3:
                myIntObject.getSqrt();
                break;

            case 4:
                myIntObject.isPrime();
                break;

            case 5:
                myIntObject.isPerfect();
                break;

            case 6:
                myIntObject.all();
                break;

            case 7:
                cout << "Please enter an integer \n";
                do {
                    cin >> userInput;
                } while (!myIntObject.validateInput(userInput));
                break;

            case 8:
                cout << "Goodbye!\n";
                return 0;

                // does not nee default statement because
                // the input option is verified
        }

        cout <<"\n\n";
    }

    return 0;
}